#include "ros/ros.h"
#include "std_msgs/Float64.h"

#include <sstream>

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "quadruped_teleop");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The advertise() function is how you tell ROS that you want to
   * publish on a given topic name. This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing. After this advertise() call is made, the master
   * node will notify anyone who is trying to subscribe to this topic name,
   * and they will in turn negotiate a peer-to-peer connection with this
   * node.  advertise() returns a Publisher object which allows you to
   * publish messages on that topic through a call to publish().  Once
   * all copies of the returned Publisher object are destroyed, the topic
   * will be automatically unadvertised.
   *
   * The second parameter to advertise() is the size of the message queue
   * used for publishing messages.  If messages are published more quickly
   * than we can send them, the number here specifies how many messages to
   * buffer up before throwing some away.
   */
  ros::Publisher frontleft_leg_j0_pub = n.advertise<std_msgs::Float64>("/quadruped/frontleft_leg_joint0_position_controller/command", 1000);
  ros::Publisher frontleft_leg_j1_pub = n.advertise<std_msgs::Float64>("/quadruped/frontleft_leg_joint1_position_controller/command", 1000);
  ros::Publisher frontleft_leg_j2_pub = n.advertise<std_msgs::Float64>("/quadruped/frontleft_leg_joint2_position_controller/command", 1000);

  ros::Publisher frontright_leg_j0_pub = n.advertise<std_msgs::Float64>("/quadruped/frontright_leg_joint0_position_controller/command", 1000);
  ros::Publisher frontright_leg_j1_pub = n.advertise<std_msgs::Float64>("/quadruped/frontright_leg_joint1_position_controller/command", 1000);
  ros::Publisher frontright_leg_j2_pub = n.advertise<std_msgs::Float64>("/quadruped/frontright_leg_joint2_position_controller/command", 1000);

  ros::Publisher rearleft_leg_j0_pub = n.advertise<std_msgs::Float64>("/quadruped/rearleft_leg_joint0_position_controller/command", 1000);
  ros::Publisher rearleft_leg_j1_pub = n.advertise<std_msgs::Float64>("/quadruped/rearleft_leg_joint1_position_controller/command", 1000);
  ros::Publisher rearleft_leg_j2_pub = n.advertise<std_msgs::Float64>("/quadruped/rearleft_leg_joint2_position_controller/command", 1000);


  ros::Publisher rearright_leg_j0_pub = n.advertise<std_msgs::Float64>("/quadruped/rearright_leg_joint0_position_controller/command", 1000);
  ros::Publisher rearright_leg_j1_pub = n.advertise<std_msgs::Float64>("/quadruped/rearright_leg_joint1_position_controller/command", 1000);
  ros::Publisher rearright_leg_j2_pub = n.advertise<std_msgs::Float64>("/quadruped/rearright_leg_joint2_position_controller/command", 1000);
 
  ros::Rate loop_rate(20);

  /**
   * A count of how many messages we have sent. This is used to create
   * a unique string for each message.
   */
  int count = 0;
  int frontleft_leg_state = 1;
  int frontright_leg_state = 2;
  int rearleft_leg_state = 0;
  int rearright_leg_state = 0;



  /**
  * This is a message object. You stuff it with data, and then publish it.
  */
  std_msgs::Float64 frontleft_leg_j0_cmd;
  std_msgs::Float64 frontleft_leg_j1_cmd;
  std_msgs::Float64 frontleft_leg_j2_cmd;

  std_msgs::Float64 frontright_leg_j0_cmd;
  std_msgs::Float64 frontright_leg_j1_cmd;
  std_msgs::Float64 frontright_leg_j2_cmd;

  std_msgs::Float64 rearleft_leg_j0_cmd;
  std_msgs::Float64 rearleft_leg_j1_cmd;
  std_msgs::Float64 rearleft_leg_j2_cmd;

  std_msgs::Float64 rearright_leg_j0_cmd;
  std_msgs::Float64 rearright_leg_j1_cmd;
  std_msgs::Float64 rearright_leg_j2_cmd;


  frontleft_leg_j0_cmd.data = 0.0;
  frontleft_leg_j1_cmd.data = 0.0;
  frontleft_leg_j2_cmd.data = 0.0;

  frontright_leg_j0_cmd.data = 0.0;
  frontright_leg_j1_cmd.data = 0.0;
  frontright_leg_j2_cmd.data = 0.0;

  rearleft_leg_j0_cmd.data = 0.0;
  rearleft_leg_j1_cmd.data = 0.0;
  rearleft_leg_j2_cmd.data = 0.0;

  rearright_leg_j0_cmd.data = 0.0;
  rearright_leg_j1_cmd.data = 0.0;
  rearright_leg_j2_cmd.data = 0.0;


  while (ros::ok())
  {
    std::cout << "--------------------------------------------------------" << std::endl;
    std::cout << "frontleft_leg_state=" << frontleft_leg_state 
              << " j1.cmd.data=" << frontleft_leg_j1_cmd.data 
              << " j2.cmd.data=" << frontleft_leg_j2_cmd.data << std::endl;
    std::cout << "frontright_leg_state=" << frontright_leg_state 
              << " j1.cmd.data=" << frontright_leg_j1_cmd.data 
              << " j2.cmd.data=" << frontright_leg_j2_cmd.data << std::endl;
    std::cout << "rearleft_leg_state=" << rearleft_leg_state 
              << " j1.cmd.data=" << rearleft_leg_j1_cmd.data 
              << " j2.cmd.data=" << rearleft_leg_j2_cmd.data << std::endl;
    std::cout << "rearright_leg_state=" << rearright_leg_state 
              << " j1.cmd.data=" << rearright_leg_j1_cmd.data 
              << " j2.cmd.data=" << rearright_leg_j2_cmd.data << std::endl;



    //---- frontleft_leg ----
    if (frontleft_leg_state == 0) {
      frontleft_leg_j0_cmd.data = 0;
      frontleft_leg_j1_cmd.data = 0;
      frontleft_leg_j2_cmd.data = 0;
    }
    else if (frontleft_leg_state== 1) {
      frontleft_leg_j0_cmd.data = 0;
      //frontleft_leg_j1_cmd.data = 0.7; //frontleft_leg_j1_cmd.data + 0.1;
      frontleft_leg_j1_cmd.data = frontleft_leg_j1_cmd.data + 0.1;
      //frontleft_leg_j2_cmd.data = -0.9; //frontleft_leg_j2_cmd.data - 0.1;
      frontleft_leg_j2_cmd.data = frontleft_leg_j2_cmd.data - 0.1;
    }
    else if (frontleft_leg_state == 2) {
      frontleft_leg_j0_cmd.data = 0;
      //frontleft_leg_j1_cmd.data = 0.9; //frontleft_leg_j1_cmd.data;
      frontleft_leg_j1_cmd.data = frontleft_leg_j1_cmd.data + 0.1;
      //frontleft_leg_j2_cmd.data = -0.1; //frontleft_leg_j2_cmd.data + 0.1;
      frontleft_leg_j2_cmd.data = frontleft_leg_j2_cmd.data + 0.2;

    }
    else if (frontleft_leg_state == 3) {
      frontleft_leg_j0_cmd.data = 0;
      //frontleft_leg_j1_cmd.data = -0.6; //frontleft_leg_j1_cmd.data - 0.1;
      frontleft_leg_j1_cmd.data = frontleft_leg_j1_cmd.data - 0.1;
      //frontleft_leg_j2_cmd.data = 0.0; //frontleft_leg_j2_cmd.data;
      frontleft_leg_j2_cmd.data = frontleft_leg_j2_cmd.data;
    }
    else if (frontleft_leg_state == 4) {
      frontleft_leg_j0_cmd.data = 0;
      //frontleft_leg_j1_cmd.data = -0.6; //frontleft_leg_j1_cmd.data;
      frontleft_leg_j1_cmd.data = frontleft_leg_j1_cmd.data;
      //frontleft_leg_j2_cmd.data = -0.9; //frontleft_leg_j2_cmd.data - 0.1;
      frontleft_leg_j2_cmd.data = frontleft_leg_j2_cmd.data - 0.1;
    }
    else {
      frontleft_leg_j0_cmd.data = 0;
      frontleft_leg_j1_cmd.data = 0;
      frontleft_leg_j2_cmd.data = 0;
    }

    // frontleft_leg_state transition logic
    if (frontleft_leg_state == 0) {
//      frontleft_leg_state = 1;
    }
    else if (frontleft_leg_state== 1) {
      if(frontleft_leg_j1_cmd.data > 0.5) {
        frontleft_leg_state = 2;
      }
    }
    else if (frontleft_leg_state == 2) {
      if((frontleft_leg_j1_cmd.data > 0.6) && 
         (frontleft_leg_j2_cmd.data > -0.3) )
        frontleft_leg_state = 3;
    }
    else if (frontleft_leg_state == 3) {
      if((frontleft_leg_j1_cmd.data < -0.4))
        frontleft_leg_state = 4;
    }
    else if (frontleft_leg_state == 4) {
      if((frontleft_leg_j1_cmd.data < -0.4) &&
         (frontleft_leg_j2_cmd.data < -0.4) )
        frontleft_leg_state = 1;
    }
    else {
      frontleft_leg_state = 0;
    }
    //---- frontleft_leg ----

    //---- frontright_leg ----
    if (frontright_leg_state == 0) {
      frontright_leg_j0_cmd.data = 0;
      frontright_leg_j1_cmd.data = 0;
      frontright_leg_j2_cmd.data = 0;
    }
    else if (frontright_leg_state== 1) {
      frontright_leg_j0_cmd.data = 0;
      //frontright_leg_j1_cmd.data = 0.7; //frontright_leg_j1_cmd.data + 0.1;
      frontright_leg_j1_cmd.data = frontright_leg_j1_cmd.data + 0.1;
      //frontright_leg_j2_cmd.data = -0.9; //frontright_leg_j2_cmd.data - 0.1;
      frontright_leg_j2_cmd.data = frontright_leg_j2_cmd.data - 0.1;
    }
    else if (frontright_leg_state == 2) {
      frontright_leg_j0_cmd.data = 0;
      //frontright_leg_j1_cmd.data = 0.9; //frontright_leg_j1_cmd.data;
      frontright_leg_j1_cmd.data = frontright_leg_j1_cmd.data + 0.1;
      //frontright_leg_j2_cmd.data = -0.1; //frontright_leg_j2_cmd.data + 0.1;
      frontright_leg_j2_cmd.data = frontright_leg_j2_cmd.data + 0.2;

    }
    else if (frontright_leg_state == 3) {
      frontright_leg_j0_cmd.data = 0;
      //fronright_leg_j1_cmd.data = -0.6; //frontright_leg_j1_cmd.data - 0.1;
      frontright_leg_j1_cmd.data = frontright_leg_j1_cmd.data - 0.1;
      //frontright_leg_j2_cmd.data = 0.0; //frontright_leg_j2_cmd.data;
      frontright_leg_j2_cmd.data = frontright_leg_j2_cmd.data;
    }
    else if (frontright_leg_state == 4) {
      frontright_leg_j0_cmd.data = 0;
      //frontright_leg_j1_cmd.data = -0.6; //frontright_leg_j1_cmd.data;
      frontright_leg_j1_cmd.data = frontright_leg_j1_cmd.data;
      //frontright_leg_j2_cmd.data = -0.9; //frontright_leg_j2_cmd.data - 0.1;
      frontright_leg_j2_cmd.data = frontright_leg_j2_cmd.data - 0.1;
    }
    else {
      frontright_leg_j0_cmd.data = 0;
      frontright_leg_j1_cmd.data = 0;
      frontright_leg_j2_cmd.data = 0;
    }

    // frontright_leg_state transition logic
    if (frontright_leg_state == 0) {
//      frontright_leg_state = 1;
    }
    else if (frontright_leg_state== 1) {
      if(frontright_leg_j1_cmd.data > 0.5) {
        frontright_leg_state = 2;
      }
    }
    else if (frontright_leg_state == 2) {
      if((frontright_leg_j1_cmd.data > 0.6) && 
         (frontright_leg_j2_cmd.data > -0.3) )
        frontright_leg_state = 3;
    }
    else if (frontright_leg_state == 3) {
      if((frontright_leg_j1_cmd.data < -0.4))
        frontright_leg_state = 4;
    }
    else if (frontright_leg_state == 4) {
      if((frontright_leg_j1_cmd.data < -0.4) &&
         (frontright_leg_j2_cmd.data < -0.4) )
        frontright_leg_state = 1;
    }
    else {
      frontright_leg_state = 0;
    }
    //---- frontright_leg ----


    //---- rearleft_leg ----
    if (rearleft_leg_state == 0) {
      rearleft_leg_j0_cmd.data = 0;
      rearleft_leg_j1_cmd.data = 0;
      rearleft_leg_j2_cmd.data = 0;
    }
    else if (rearleft_leg_state== 1) {
      rearleft_leg_j0_cmd.data = 0;
      //rearleft_leg_j1_cmd.data = 0.7; //frontleft_leg_j1_cmd.data + 0.1;
      rearleft_leg_j1_cmd.data = rearleft_leg_j1_cmd.data + 0.1;
      //rearleft_leg_j2_cmd.data = -0.9; //frontleft_leg_j2_cmd.data - 0.1;
      rearleft_leg_j2_cmd.data = rearleft_leg_j2_cmd.data - 0.1;
    }
    else if (rearleft_leg_state == 2) {
      rearleft_leg_j0_cmd.data = 0;
      //rearleft_leg_j1_cmd.data = 0.9; //frontleft_leg_j1_cmd.data;
      rearleft_leg_j1_cmd.data = rearleft_leg_j1_cmd.data + 0.1;
      //frontleft_leg_j2_cmd.data = -0.1; //frontleft_leg_j2_cmd.data + 0.1;
      rearleft_leg_j2_cmd.data = rearleft_leg_j2_cmd.data + 0.2;

    }
    else if (rearleft_leg_state == 3) {
      rearleft_leg_j0_cmd.data = 0;
      //rearleft_leg_j1_cmd.data = -0.6; //frontleft_leg_j1_cmd.data - 0.1;
      rearleft_leg_j1_cmd.data = rearleft_leg_j1_cmd.data - 0.1;
      //frontleft_leg_j2_cmd.data = 0.0; //frontleft_leg_j2_cmd.data;
      rearleft_leg_j2_cmd.data = rearleft_leg_j2_cmd.data;
    }
    else if (rearleft_leg_state == 4) {
      rearleft_leg_j0_cmd.data = 0;
      //rearleft_leg_j1_cmd.data = -0.6; //frontleft_leg_j1_cmd.data;
      rearleft_leg_j1_cmd.data = rearleft_leg_j1_cmd.data;
      //rearleft_leg_j2_cmd.data = -0.9; //frontleft_leg_j2_cmd.data - 0.1;
      rearleft_leg_j2_cmd.data = rearleft_leg_j2_cmd.data - 0.1;
    }
    else {
      rearleft_leg_j0_cmd.data = 0;
      rearleft_leg_j1_cmd.data = 0;
      rearleft_leg_j2_cmd.data = 0;
    }

    // rearleft_leg_state transition logic
    if (rearleft_leg_state == 0) {
//      rearleft_leg_state = 1;
    }
    else if (rearleft_leg_state== 1) {
      if(rearleft_leg_j1_cmd.data > 0.5) {
        rearleft_leg_state = 2;
      }
    }
    else if (rearleft_leg_state == 2) {
      if((rearleft_leg_j1_cmd.data > 0.6) && 
         (rearleft_leg_j2_cmd.data > -0.3) )
        rearleft_leg_state = 3;
    }
    else if (rearleft_leg_state == 3) {
      if((rearleft_leg_j1_cmd.data < -0.4))
        rearleft_leg_state = 4;
    }
    else if (rearleft_leg_state == 4) {
      if((rearleft_leg_j1_cmd.data < -0.4) &&
         (rearleft_leg_j2_cmd.data < -0.4) )
        rearleft_leg_state = 1;
    }
    else {
      rearleft_leg_state = 0;
    }
    //---- rearleft_leg ----

    //---- rearright_leg ----
    if (rearright_leg_state == 0) {
      rearright_leg_j0_cmd.data = 0;
      rearright_leg_j1_cmd.data = 0;
      rearright_leg_j2_cmd.data = 0;
    }
    else if (rearright_leg_state== 1) {
      rearright_leg_j0_cmd.data = 0;
      //rearright_leg_j1_cmd.data = 0.7; //frontright_leg_j1_cmd.data + 0.1;
      rearright_leg_j1_cmd.data = rearright_leg_j1_cmd.data + 0.1;
      //rearright_leg_j2_cmd.data = -0.9; //frontright_leg_j2_cmd.data - 0.1;
      rearright_leg_j2_cmd.data = rearright_leg_j2_cmd.data - 0.1;
    }
    else if (rearright_leg_state == 2) {
      rearright_leg_j0_cmd.data = 0;
      //rearright_leg_j1_cmd.data = 0.9; //frontright_leg_j1_cmd.data;
      rearright_leg_j1_cmd.data = rearright_leg_j1_cmd.data + 0.1;
      //rearright_leg_j2_cmd.data = -0.1; //frontright_leg_j2_cmd.data + 0.1;
      rearright_leg_j2_cmd.data = rearright_leg_j2_cmd.data + 0.2;

    }
    else if (rearright_leg_state == 3) {
      rearright_leg_j0_cmd.data = 0;
      //fronright_leg_j1_cmd.data = -0.6; //rearright_leg_j1_cmd.data - 0.1;
      rearright_leg_j1_cmd.data = rearright_leg_j1_cmd.data - 0.1;
      //rearright_leg_j2_cmd.data = 0.0; //frontright_leg_j2_cmd.data;
      rearright_leg_j2_cmd.data = rearright_leg_j2_cmd.data;
    }
    else if (rearright_leg_state == 4) {
      rearright_leg_j0_cmd.data = 0;
      //rearright_leg_j1_cmd.data = -0.6; //frontright_leg_j1_cmd.data;
      rearright_leg_j1_cmd.data = rearright_leg_j1_cmd.data;
      //rearright_leg_j2_cmd.data = -0.9; //frontright_leg_j2_cmd.data - 0.1;
      rearright_leg_j2_cmd.data = rearright_leg_j2_cmd.data - 0.1;
    }
    else {
      rearright_leg_j0_cmd.data = 0;
      rearright_leg_j1_cmd.data = 0;
      rearright_leg_j2_cmd.data = 0;
    }

    // rearright_leg_state transition logic
    if (rearright_leg_state == 0) {
//      rearright_leg_state = 1;
    }
    else if (rearright_leg_state== 1) {
      if(rearright_leg_j1_cmd.data > 0.5) {
        rearright_leg_state = 2;
      }
    }
    else if (rearright_leg_state == 2) {
      if((rearright_leg_j1_cmd.data > 0.6) && 
         (rearright_leg_j2_cmd.data > -0.3) )
        rearright_leg_state = 3;
    }
    else if (rearright_leg_state == 3) {
      if((rearright_leg_j1_cmd.data < -0.4))
        rearright_leg_state = 4;
    }
    else if (rearright_leg_state == 4) {
      if((rearright_leg_j1_cmd.data < -0.4) &&
         (rearright_leg_j2_cmd.data < -0.4) )
        rearright_leg_state = 1;
    }
    else {
      rearright_leg_state = 0;
    }
    //---- rearright_leg ----



//    ROS_INFO("%s", cmd.data.c_str());

    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     */
    frontleft_leg_j0_pub.publish(frontleft_leg_j0_cmd);
    frontleft_leg_j1_pub.publish(frontleft_leg_j1_cmd);
    frontleft_leg_j2_pub.publish(frontleft_leg_j2_cmd);

    frontright_leg_j0_pub.publish(frontright_leg_j0_cmd);
    frontright_leg_j1_pub.publish(frontright_leg_j1_cmd);
    frontright_leg_j2_pub.publish(frontright_leg_j2_cmd);

    rearleft_leg_j0_pub.publish(rearleft_leg_j0_cmd);
    rearleft_leg_j1_pub.publish(rearleft_leg_j1_cmd);
    rearleft_leg_j2_pub.publish(rearleft_leg_j2_cmd);

    rearright_leg_j0_pub.publish(rearright_leg_j0_cmd);
    rearright_leg_j1_pub.publish(rearright_leg_j1_cmd);
    rearright_leg_j2_pub.publish(rearright_leg_j2_cmd);


    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }


  return 0;
}
